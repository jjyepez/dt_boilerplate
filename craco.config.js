const CracoLessPlugin = require("craco-less");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { 
              "@primary-color": "#336699",
              // "@ligth-grey": "#d9d9d9",
              // "dark-grey": "#bfbfbf",
              // "@size-small": ".5em",
              // "@size-medium": "1em",
              // "@size-large": "2em"
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
