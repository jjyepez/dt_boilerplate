import React from "react";

import LayoutContainer from "../components/LayoutContainer";
import LoginFormFragment from "../fragments/LoginFormFragment";

import './css/LoginPage.less';

const LoginPage = () => {

    return (

        <LayoutContainer className="LoginPage">
        
            <LoginFormFragment/>
        
        </LayoutContainer>
    
    )
}

export default LoginPage;