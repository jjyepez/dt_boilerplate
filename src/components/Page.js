import React from 'react';

import { Layout } from 'antd';

import './css/Page.less';

const Page = (props) => {

    return(
        <Layout.Content className="Page">
            
            {props.children}

        </Layout.Content>
    )
}

export default Page;