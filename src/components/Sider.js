import React, { useState } from "react";
import { Link } from "react-router-dom";

import { Menu, Layout, Button } from "antd";

import { SettingOutlined, MenuOutlined } from '@ant-design/icons';

import './css/Sider.less';

const Sider = ({ menuItems }) => {

  const [collapsed, setCollapsed] = useState(false);

  return (

    <Layout.Sider
      className="Sider"
      collapsible
      // trigger={null} 
      // collapsedWidth={0}
      // collapsed={collapsed}
    >

      <Menu
        mode="inline"
        inlineCollapsed={true}
      >

        { menuItems.map((menuItem, i) => {
          return (
            <Menu.Item key={i} icon={<SettingOutlined />}>

              <Link to={menuItem.route}>{menuItem.text}</Link>
            
            </Menu.Item>
          );
        }) }
    
      </Menu>
    
    </Layout.Sider>
  
  )

}

export default Sider;
