import React from "react";
import { Link, withRouter } from "react-router-dom";
import { Layout, Menu } from "antd";

import LoginFragment from "../fragments/LoginFragment";
import Logo from './Logo';

import "./css/MenuBar.less";

const MenuBar = ({ menuItems, isLogged }) => {

  return (

    <Layout.Header theme="dark" className="MenuBar">

      <Logo theme="dark"/>

      <Menu theme="dark" mode="horizontal">

        { menuItems
            .filter(
              item => !item.needsLogin || (item.needsLogin && isLogged)
            )
            .map((item, i) => {
              return (

                <Menu.Item key={`menu-${i}`}>
              
                  <Link to = { item.route }>
                      { item.text }
                  </Link>
              
                </Menu.Item>
                
              )
        })}

        <div className="FloatRight">
            
            <LoginFragment />
        
      </div>

      </Menu>

    </Layout.Header>
  )

}

export default withRouter( MenuBar );
