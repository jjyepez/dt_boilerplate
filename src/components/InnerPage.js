import React from 'react';

import { Layout } from 'antd';

import './css/InnerPage.less';

const InnerPage = (props) => {

    return(
        <Layout.Content className="InnerPage">
            
            {props.children}

        </Layout.Content>
    )
}

export default InnerPage;