import React from 'react';

import './css/Logo.less';

const Logo = ({theme}) => {

    let appTitle = 'System Name';

    return(
        <div className={`Logo ${theme}`}>
            {appTitle}
        </div>
    )
}

export default Logo;