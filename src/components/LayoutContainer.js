import React from "react";
import { Layout } from "antd";

import "./css/LayoutContainer.less";

const LayoutContainer = ({ children }) => {

  return (

    <Layout className="LayoutContainer">

      <Layout.Content className="Content">
        {children}
      </Layout.Content>
    
    </Layout>
  
  )

}

export default LayoutContainer;
