import React from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";
import "./css/LoginHeader.less";

const LoginHeader = ({ userInfo, isLogged, handlerLogIn }) => {

  return isLogged ? (
    <div className="LoginHeader">
  
      <label className="UserName">{userInfo.name}</label>
      
      <Button
        type="primary"
        // shape = "round"
        className="Button"
        onClick={() => handlerLogIn()}
      >
        Logout
      </Button>
    </div>
  
  ) : (
  
      window.location.pathname !== '/login' && 
      (
        <div className="LoginHeader">

          <Link to="/login">
            
            <Button
              type="primary"
              className="Button"
            >
              Iniciar Sesión
            </Button>
          
          </Link>
        
        </div>
      )
  )
}

export default LoginHeader;
