import React from "react";
import { Table, Typography } from "antd";

import InnerPage from "../components/InnerPage";
import CrudToolbar from '../fragments/CrudToolbar'; 

import "./css/SubModuleContentFragment.less";

const SubModuleContentFragment = ({ title, columns, data }) => {
  
  return (
    
    <InnerPage className="SubModuleContentFragment">

      <Typography.Title level={4} className="PageHeader">
        {title}
      </Typography.Title>
      
      <CrudToolbar
        buttons = {["new"]}
      />

      <Table
        size       = "small"
        columns    = {columns}
        dataSource = {data}
      />
    
    </InnerPage>
  
  )

}

export default SubModuleContentFragment;
