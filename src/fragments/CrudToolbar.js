import React from 'react';

import { Button } from 'antd';

import './css/CrudToolbar.less';

const CrudToolbar = ({buttons}) => {
    return (
        <div className="CrudToolbar">
            <Button size="middle" type="primary">Nuevo item</Button>
        </div>
    )
}

export default CrudToolbar;