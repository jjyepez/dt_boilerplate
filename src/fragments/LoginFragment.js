import React, { useContext, useState, useEffect } from "react";

import LoginHeader from "../components/LoginHeader";
import { Context } from "../Context";

const LoginFragment = () => {

  const { isLogged, removeLogin } = useContext(Context);
  const [user, setUser] = useState([])

  useEffect(() => {
      if(isLogged){
        setTimeout(() => {
          setUser(JSON.parse((window.localStorage.getItem("user"))))
        }, 400);
      }
  }, [isLogged])

  return (

      <LoginHeader
        isLogged={isLogged}
        userInfo={{
          name: !user.length && user.nombres,
          rol: !user.length &&  user.rolName,
        }}
        handlerLogIn={removeLogin}
      />
  )

}

export default LoginFragment;
