import React from "react";

import Page from '../components/Page';

const ModuleContentFragment = (props) => {

  return (

    <Page>
      {props.children}
    </Page>
  
  )

}

export default ModuleContentFragment;
