import React, { useState, useContext } from 'react';
import { Context } from "../Context";
import { useHistory } from "react-router-dom";
import { Row, Col,Form, Input, Button, Space, Typography, Alert, Card } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

import './css/LoginFormFragment.less';

import Page from '../components/Page';

const LoginForm = () => {

    let history = useHistory();
    const { setLogin } = useContext(Context);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    const onFinish = async values => {
        setLoading(true);
        try {

            const auth = await fetch("http://localhost:8100/api/services/auth", {
                method: "POST",
                body: JSON.stringify(values),
                headers:{
                    'Content-Type': 'application/json'
                }
            })

            const result = await auth.json();
            if(result.error){
                throw `${result.message}`
            }
            
            setLoading(false);
            setLogin(result.data.token, result.data.user);

            history.push("/");
            
        } catch (error) {
            setError(error)
            setLoading(false)
        }
    };

  return (

    <Page>

        <div className="LoginFormFragment">

            <Card className="Card" title="Login">

                <Form
                    className="LoginForm"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                >
                    {
                        error &&  <Alert message={error} type="warning" showIcon closable />
                    }

                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: 'Please input your Username!' }]}
                    >
                        <Input prefix={<UserOutlined 
                            className="site-form-item-icon" />} 
                            placeholder="Username" 
                            onChange={() => setError('')}/>

                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                    >
                        <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="Password"
                            onChange={() => setError('')}
                        />

                    </Form.Item>

                    <Space direction="vertical" className="Space">

                        <Button type="primary" loading={loading}  htmlType="submit" className="login-form-button">
                            Iniciar Sesión
                        </Button>

                        <Button type="default" htmlType="" className="login-form-button">
                            Recuperar contaseña
                        </Button>

                    </Space>

                </Form>
            
            </Card>
        
        </div> 
    
    </Page>

  )

}

export default LoginForm;