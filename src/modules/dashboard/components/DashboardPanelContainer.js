import React from "react";
import { Card, Row, Col } from "antd";

import "./css/DashboardPanelContainer.less";

const DashboardPanelContainer = ({ events }) => {

  return (
    
    <div className="DashboardPanelContainer">

      <Row className="DashboardRow" gutter={[15, 15]}>   

        <Col span={12} flex={1}>
          <Card className='DashboardPanel'>
            Lorem
          </Card>
        </Col>

        <Col span={12} flex={1}>
          <Card className='DashboardPanel'>
            Lorem
          </Card>
        </Col>
      
      </Row>

      <Row className="DashboardRow" gutter={[15, 0]}>
           
        <Col span={12} flex={1}>
          <Card className='DashboardPanel'>
            Lorem
          </Card>
        </Col>

        <Col span={12} flex={1}>
          <Card className='DashboardPanel'>
            Lorem
          </Card>
        </Col>
      
      </Row>

    </div>
  )

}

export default DashboardPanelContainer;
