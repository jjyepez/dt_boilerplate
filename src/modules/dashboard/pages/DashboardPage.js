import React from "react";

import Page from '../../../components/Page';
import DashboardPanelContainer from "../components/DashboardPanelContainer";

import "./css/DashboardPage.less";

const DashboardPage = () => {
  const eventsPm2 = [
    {
      id: "1",
      title: "Estado de Listener GPS(AVL)",
      listeners: [
        {
          name: "Listener Syrus",
          status: "on",
          port: "Escuchando en el puerto 9021",
          time: "5 minutos",
        },
        {
          name: "Listener GV55",
          status: "on",
          port: "Escuchando en el puerto 9022",
          time: "5 minutos",
        },
        {
          name: "Listener Trax",
          status: "off",
          port: "Escuchando en el puerto 9023",
          time: "50 minutos",
        },
      ],
    },
    {
      id: "2",
      title: "Estado de Listener Radios(APL)",
      listeners: [
        {
          name: "Listener Radios TIPO A",
          status: "on",
          port: "Escuchando en el puerto 14021",
          time: "5 minutos",
        },
        {
          name: "Listener Radios TIPO B",
          status: "on",
          port: "Escuchando en el puerto 14021",
          time: "5 minutos",
        },
      ],
    },
    {
      id: "3",
      title: "Estado de comunicación con PremierOne",
      listeners: [
        {
          name: "Enviados a PremierOne",
          status: "off",
          port: "Emitiendo al puerto 8010",
          time: "5 minutos",
        },
        {
          name: "Recibidos a PremierOne",
          status: "on",
          port: "Escuchando en el puerto 7010",
          time: "5 minutos",
        },
      ],
    },
    {
      id: "4",
      title: "",
      systemStates: [
        {
          title: "Estado del sistema",
          actions: [
            {
              date: "Mie 15/04/2019 16:30",
              user: "administrador",
              action: "Vehículo creado",
            },
          ],
        },
        {
          title: "Acciones recientes",
          actions: [
            {
              date: "Mie 15/04/2019 16:30",
              user: "administrador",
              action: "Vehículo creado",
            },
            {
              date: "Mie 15/04/2019 16:30",
              user: "administrador",
              action: "Usuario consultor modificado",
            },
            {
              date: "Mie 15/04/2019 16:30",
              user: "administrador",
              action: "Usuario consultor modificado",
            },
          ],
        },
      ],
    },
  ];

  return (
    
    <Page className="DashboardPage">
      
      <DashboardPanelContainer events={eventsPm2} />
    
    </Page>
  );
};

export default DashboardPage;
