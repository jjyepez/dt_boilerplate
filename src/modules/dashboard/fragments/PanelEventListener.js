import React from "react";

import { Row, Card } from "antd";

import "./css/PanelEventListener.less";

const StatusType = (value) => {
  switch (value) {
    case "on":
      return "Escuchando";
    case "off":
      return "Apagado";
    default:
      break;
  }
};

const PanelEventListener = ({ name, port, time, status }) => {

  return (
    <Card>
      
      <Row justify="space-between">
        <h4 className="Title">{name}</h4>
        <span className="Status">
          {StatusType(status)} <i className={`StatusCircle ${status}`}></i>
        </span>
      </Row>

      <div className="Details">
        <p>{port}</p>
        <p>Último reporte {time}</p>
      </div>
    
    </Card>

  )

}

export default PanelEventListener;
