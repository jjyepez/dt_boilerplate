import React from "react";

import "./css/PanelSystemState.less";

import { Typography, Card } from "antd";

const PanelSystemState = ({ index, title, actions }) => {

  return (

    <Card>

      <Typography.Title level={4}>{title}</Typography.Title>
      
      {actions.map((action, i) => {
        {
          if (index === 0) {
            return (
              <div key={i} className="Actions">
                <p>Último acceso: {action.date}</p>
                <p>Usuario último administrador {action.user}</p>
                <p>Última acción realizada: {action.action} </p>
              </div>
            );
          } else {
            return (
              <div className="Actions" key={i}>
                <p>
                  {action.date} {action.action} {action.user}
                </p>
              </div>
            );
          }
        }
      })}
      
    </Card>
  
  )

}

export default PanelSystemState;
