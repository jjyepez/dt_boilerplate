import React from "react";

import LayoutContainer from "../../components/LayoutContainer";
import DashboardPage from "./pages/DashboardPage";

const DashBoardModule = () => {
  
  return (
    
    <LayoutContainer>
    
      <DashboardPage />
    
    </LayoutContainer>

  )

}

export default DashBoardModule;
