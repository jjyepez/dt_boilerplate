import React from "react";

import LayoutContainer from "../../components/LayoutContainer";
import AdminPage from "./pages/AdminPage";

const AdminModule = () => {

    return (
        <LayoutContainer>

            <AdminPage />
        
        </LayoutContainer>
    )

}

export default AdminModule;