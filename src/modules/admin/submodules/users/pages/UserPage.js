import React, { useEffect, useState } from "react";
import SubModuleContentFragment from "../../../../../fragments/SubModuleContentFragment";

import InnerPage from '../../../../../components/InnerPage';

const DevicesPages = () => {

  const [users, setUsers] = useState([]);

  useEffect(() => {

     async function fetchData(){
       
        const data = await fetch("http://localhost:8100/api/services/usuarios")
          .then(rslt => rslt.json() );

        const result = data.data.map((user,i) =>({
            key: i,
            username: user.user,
            fullName: `${user.nombres} ${user.apellidos}`,
            rol: user.rolName
        }));

        setUsers(result);
      }
      
      fetchData();
  
    }, [])

  const columns = [
    {
      title: "Nombre Usuario",
      dataIndex: "username",
    },
    {
      title: "Nombre Completo",
      dataIndex: "fullName",
    },
    {
      title: "Rol",
      dataIndex: "rol",
    },
    {
      title: "",
      dataIndex: "",
    }
  ]
      
  return (

    <SubModuleContentFragment title="Usuarios" columns={columns} data={users} />
  
  )

}

export default DevicesPages;

