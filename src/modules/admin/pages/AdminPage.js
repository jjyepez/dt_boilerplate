import React from 'react';
import { BrowserRouter, Redirect } from 'react-router-dom';

import Sider from '../../../components/Sider';
import {
    Row,
    Col,
    Layout
} from 'antd';

import './css/AdminPage.less';

import ModuleContentFragment from '../../../fragments/ModuleContentFragment';
import UserPage from "../submodules/users/pages/UserPage"

import {
    Switch,
    Route,
} from 'react-router-dom';

const AdminPage = () => {

    const getMenuItems = [
            {
                text: 'Usuarios',
                route: '/admin/users',
                component: <UserPage />
            },
            {
                text: 'Utilidades',
                route: '/admin/utils',
                components: <div>UTILS</div>
            },
        ]

    return (
        <Layout.Content>

            <BrowserRouter className="AdminPage">

                <Row>
                    
                    <Col>
                        <Sider menuItems = { getMenuItems } />
                    </Col>

                    <Col flex={1}>

                        <ModuleContentFragment>
                            
                            <Switch>

                                { getMenuItems.map((item, i) => {

                                    return(
                                    
                                        <Route key={i} path= {item.route} exact>
                                            {item.component}
                                        </Route>
                                    )

                                })}

                                <Redirect to={getMenuItems[0].route} />
                            
                            </Switch>
                    
                        </ModuleContentFragment>

                    </Col>
                
                </Row>
            
            </BrowserRouter>

        </Layout.Content>
    )
}

export default AdminPage;