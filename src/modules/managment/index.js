import React from "react";

import LayoutContainer from "../../components/LayoutContainer";
import ManagmentPage from "./pages/ManagmentPage";

const ManagmentModule = () => {

  return (
    
    <LayoutContainer>

      <ManagmentPage />
   
    </LayoutContainer>

)

}

export default ManagmentModule;
