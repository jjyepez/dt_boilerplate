import React from "react";
import Sider from "../../../components/Sider";

import { Switch, Route, BrowserRouter, Redirect } from "react-router-dom";

import DevicesPages from "../submodules/devices/pages/DevicesPages";
import VehiclesPages from "../submodules/vehicles/pages/VehiclesPages";
import ModuleContentFragment from "../../../fragments/ModuleContentFragment";

import { Row, Col, Layout } from "antd";

import "./css/ManagmentPage.less";

const ManagmentPage = () => {

  const getMenuItems = [
    {
      text: "Dispositivos",
      route: "/gestion/dispositivos",
      default: true,
    },
    {
      text: "Vehículos",
      route: "/gestion/vehiculos",
    }
  ];

  return (
    <BrowserRouter>

      <Layout.Content>
    
        <Row>

          <Col>
            <Sider menuItems={getMenuItems} />
          </Col>
          
          <Col flex={1}>

            <ModuleContentFragment>
              
              <Switch>

                <Route exact path="/gestion/dispositivos"t>
                  <DevicesPages />
                </Route>

                <Route exact path="/gestion/vehiculos">
                  <VehiclesPages />
                </Route>

                <Redirect to="/gestion/dispositivos" />
                
              </Switch>
            
            </ModuleContentFragment>
          
          </Col>

        </Row>

      </Layout.Content>

    </BrowserRouter>

  )

}

export default ManagmentPage;
