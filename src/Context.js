import React, {createContext, useState} from "react"

export const Context = createContext();

const Provider = ({ children }) => { 
    
    const [isLogged, handleLogIn] = useState(() => {
        return window.localStorage.getItem("token")
    })

    const [userInfo, setUserInfo] = useState(() => {
        return window.localStorage.getItem("user")
    })

    const value = {
        isLogged,
        userInfo,
        setLogin: (token, user) => {
            const userString = JSON.stringify(user)
            handleLogIn(true)
            window.localStorage.setItem('token', token)
            window.localStorage.setItem('user', userString)
        },
        removeLogin: () => {
            handleLogIn(false)
            localStorage.clear()
        }
    }
    
    return <Context.Provider value={value}>{children}</Context.Provider>;
}

export default {
    Provider
};
  