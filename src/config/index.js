import React from 'react';
import DashboardModule from '../modules/dashboard';
import ManagmentModule from "../modules/managment";
import AdminModule from '../modules/admin';


export default {

    MainMenu: [
        {
          route: '/dashboard',
          text: 'Dashboard',
          component: <DashboardModule />,
        },
        {
          route: '/admin',
          text: 'Administración',
          component: <AdminModule />,
          needsLogin: true
        },
        {
          route: '/gestion',
          text: 'Gestión',
          component: <ManagmentModule />,
          needsLogin: true
        }
    ]
}