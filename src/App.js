import React, { useContext } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import LayoutContainer from './components/LayoutContainer';
import MenuBar from "./components/MenuBar";
import configRoutes from './config';

import LoginPage from "./pages/LoginPage"
import { Context } from "./Context";

import "./App.less";

const App = () => {

  const { isLogged, isCollapsed } = useContext( Context );

  const defineMenuItems = () => {
    return configRoutes.MainMenu;
  }

  return (
      <BrowserRouter>
        
          <LayoutContainer>

              <MenuBar 
                isLogged = { isLogged }
                menuItems = { defineMenuItems() }
              />

                <Switch>
             
                  <Route path="/login" exact component={ LoginPage } />     
              
                  { defineMenuItems()
                    .map((item, i) => {

                      return (

                        <Route key={i} path={ item.route } exact>

                          { item.component }
                        
                        </Route>
                      
                      )

                    }
                  ) }
              
                  <Redirect to = '/dashboard' />
                
                </Switch>

          </LayoutContainer>

    </BrowserRouter>
    
  )

}

export default App;

